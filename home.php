<?php get_header(); // Loads the header.php template. ?>

	<div id="content" class="hfeed">

		<?php get_template_part( 'breadcrumbs' ); // Loads the breadcrumbs.php template. ?>

		<?php if ( printing_shop_is_blog_home()  ) { // only show home page widgets if blog is front page ?>			

			<?php get_sidebar( 'home-page-one' ); // Loads the sidebar-home-page-one.php template. ?>

			<?php get_sidebar( 'home-page-two' ); // Loads the sidebar-home-page-two.php template. ?>

			<?php get_sidebar( 'home-page-three' ); // Loads the sidebar-home-page-three.php template. ?>

		<?php } // endif is_front_page() ?>

		<?php get_template_part( 'loop-meta' ); // Loads the loop-meta.php template. ?>

		<?php get_template_part( 'loop' ); // Loads the loop.php template. ?>

		<?php get_template_part( 'loop-nav' ); // Loads the loop-nav.php template. ?>

	</div><!-- #content -->

<?php get_footer(); // Loads the footer.php template. ?>