
<?php if ( is_active_sidebar( 'home-page-two' ) ) { ?>

	<section id="sidebar-home-two" class="sidebar">

		<?php dynamic_sidebar( 'home-page-two' ); ?>

	</section><!-- #sidebar-home-two -->

<?php } ?>