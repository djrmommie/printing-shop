<?php if ( is_active_sidebar( 'primary' ) ) { ?>

	<aside id="sidebar-primary" class="sidebar primary-sidebar">

		<?php dynamic_sidebar( 'primary' ); ?>

	</aside><!-- #sidebar-primary .aside -->

<?php } ?>