<?php
/**
 * The functions file is used to initialize everything in the theme.  It controls how the theme is loaded and 
 * sets up the supported features, default actions, and default filters.  If making customizations, users 
 * should create a child theme and make changes to its functions.php file (not this one).  Friends don't let 
 * friends modify parent theme files. ;)
 *
 * Child themes should do their setup on the 'after_setup_theme' hook with a priority of 11 if they want to
 * override parent theme features.  Use a priority of 9 if wanting to run before the parent theme.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write 
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * @package    PrintingShop
 * @subpackage Functions
 * @version    1.2.3-beta.3
 * @since      1.0.0
 * @author     Jenny Ragan <jenny@jennyragan.com>
 * @author     Alicia Hylton <info@alluregraphicdesign.com>
 * @copyright  Copyright (c) 2013, Jenny Ragan, Alicia Hylton
 * @link       http://djrthemes.com/themes/printingshop/
 * @link       http://themehybrid.com/themes/printing-shop
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Based on Hybrid Base, Copyright (c) 2013, Justin Tadlock  <justin@justintadlock.com>
 */

/* Load the core theme framework. */
require_once( trailingslashit( get_template_directory() ) . 'library/hybrid.php' );
new Hybrid();

/* Load tgm-plugin-activation */
require_once( trailingslashit( get_template_directory() ) . 'inc/theme-addons.php' );

/* Do theme setup on the 'after_setup_theme' hook. */
add_action( 'after_setup_theme', 'printing_shop_theme_setup' );

/* Load additional libraries a little later. */
add_action( 'after_setup_theme', 'printing_shop_load_libraries', 15 );

/**
 * Theme setup function.  This function adds support for theme features and defines the default theme
 * actions and filters.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function printing_shop_theme_setup() {

	/* Get action/filter hook prefix. */
	$prefix = hybrid_get_prefix();

	/* general theme hooks and filters */
	require_once( trailingslashit( get_template_directory() ) . 'inc/theme-setup.php' );

	/* General theme functions */
	require_once( trailingslashit( get_template_directory() ) . 'inc/theme-functions.php' );

	/* Include mods and customizer settings */
	require_once( trailingslashit( get_template_directory() ) . 'inc/theme-options.php' );
}

function printing_shop_load_libraries() {	

	/** custom version of hybrid-core theme-fonts **/
	require_once( trailingslashit( get_template_directory() ) . 'inc/djr-theme-fonts.php' );

	/* Include woocommerce if activated */
	if ( class_exists( 'Woocommerce' ) ) {
		require_once( trailingslashit( get_template_directory() ) . 'inc/addon-woocommerce.php' );
	}

	/* Include soliloquy if activated */
	if ( function_exists( 'soliloquy_slider' ) ) {
		require_once( trailingslashit( get_template_directory() ) . 'inc/addon-soliloquy.php' );
	}
}
