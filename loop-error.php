	<article <?php hybrid_post_attributes(); ?>>

		<header class="entry-header">
			<h1 class="entry-title"><?php _e( 'Nothing found', 'printing-shop' ); ?></h1>
		</header>

		<div class="entry-content">
			<p><?php _e( 'Apologies, but no entries were found.', 'printing-shop' ); ?></p>
		</div><!-- .entry-content -->

	</article><!-- .hentry .error -->