=== Printing Shop ===
Contributors: Jenny Ragan, Alicia Hylton
Tags: one-column, two-columns, right-sidebar, left-sidebar flexible-width, custom-background, custom-menu, featured-images, full-width-template, post-formats, theme-options, threaded-comments, translation-ready
Requires at least: 4.1

== Description ==

A lot of online printer websites are too busy and complicated to navigate.  Printing Shop boasts a beautiful, contemporary store for online printing shops or other storefronts with WordPress and WooCommerce.

== Features ==

HTML5 and CSS3
Responsive design for desktop and mobile
Layout Styles (you can set these globally and on a per-post basis) – 1 column or 2 Columns (content/sidebar or sidebar/content)
Customizeable home page
Multiple page and post templates
Integrates its theme options fully into the WordPress theme customizer
Logo uploader
Custom background ready
Custom widgets
Post formats support
Flexslider featured image slideshow on home page
Drop down menus
Numbered pagination
Breadcrumbs
Font options
Localization-ready (for translation into foreign languages)
Child theme sample included
Built on Hybrid Core http://themehybrid.com

== Plugin Ready ==

WooCommerce
Ninja Forms
Soliloquy Slider
Grid Columns
Whistles

== Copyright and License ==

The following resources are included within the theme package.

Hybrid Core, Copyright (c) 2008 - 2013, Justin Tadlock <justin@justintadlock.com>
Source: http://themehybrid.com/hybrid-core
License: GNU General Public License, v2 (or newer)
License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Respond.js, Copyright 2011: Scott Jehl, scottjehl.com
Source: https://github.com/scottjehl/Respond
License: Dual licensed under the MIT or GPL Version 2 licenses
License URI: /js/respond/README.md

HTML5 Shiv, @afarkas @jdalton @jon_neal @rem | 
Source: https://github.com/aFarkas/html5shiv
License: MIT/GPL2 License
License URI: /js/html5shiv/html5shiv.js

Unsemantic, Nathan Smith, sonspring.com
Source: http://unsemantic.com
License: Licensed under GPL and MIT
License URI: http://www.gnu.org/licenses/gpl.html, http://www.opensource.org/licenses/mit-license.php

Magnific Popup, Dmitry Semenov, dimsemenov.com
Source: https://github.com/dimsemenov/Magnific-Popup
License: MIT
License URI: http://www.opensource.org/licenses/mit-license.php

TGM-Plugin-Activation, Thomas Griffin <thomas@thomasgriffinmedia.com>, Gary Jones <gamajo@gamajo.com>
Source: https://github.com/thomasgriffin/TGM-Plugin-Activation
License: GNU General Public License, v2 (or newer)
License URI: http://opensource.org/licenses/gpl-2.0.php GPL v2 or later

Font Awesome, Dave Gandy
Source: http://fontawesome.io
License: SIL OFL 1.1 (Fonts), MIT License (Code)
License URI: http://fontawesome.io/license/

Flexslider, Copyright (c) 2012 WooThemes
Source: https://github.com/woothemes/FlexSlider
License: GNU General Public License, v2 (or newer)
License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

All other resources and theme elements are licensed under the GNU GPL (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html), version 2 or later.

Copyright 2014 Jenny Ragan, Alicia Hylton

== Installation ==

Read full documentation online at http://djrthemes.com/documentation/printing-shop/

Manual installation:

1. Upload the `printing-shop` folder to the `/wp-content/themes/` directory.

Installation using "Add New Theme":

1. Open your WordPress dashboard.
2. Go to Appearance > Themes. Click Install Themes tab, located at the top of the screen.
3. Click Upload.
4. Choose printing-shop.zip to upload.
5. Click Install Now.

Activiation

1. Activate the Theme through the 'Themes' menu in WordPress

== Changelog ==

= 1.2.2 =
Security Update - More info at https://make.wordpress.org/plugins/2015/04/20/fixing-add_query_arg-and-remove_query_arg-usage/
Fixed - Update TGMPA plugin installer
Fixed - Product reviews form

= 1.2.1 =
Fixed - width of Soliloquy slider on home page
Fixed - various PHP warnings
Fixed - css fix for menu hover bug
Fixed - colors for alt rows in WooCommerce tables
Fixed - layout bug in footer
Fixed - layout of product attributes in cart
Fixed - css for * for required fields in products
Fixed - 404 for missing Soliloquy stylesheet
Updated - parameters for woocommerce_related_products hook
Updated - Unsemantic grid
Updated - FontAwesome 4.2.0
Updated - Hybrid Core
Added - filters for adding custom color schemes
Added - additional context for shopping cart widget translation strings
Added - styles for fancy product builder plugin

= 1.1.0 =
Updated Soliloquy plugin integration for version 2
Fixed widget format for 3.9 Widget Customizer support
Updated TGM-Plugin-Activation to version 2.4.0
Fixed incorrect custom header support

= 1.0.3 =
Added plugins to recommended - Widget Importer & Exporter, User Status Shortcode
Fixed alignment of navigation menu in primary sidebar

= 1.0.2 =
Fixed bug that prevents social menu from being set
Fixed bug in printing shop box widget, links not being output
Updated theme info in style.css

= 1.0.0 =
Initial Release.