/* javascript for theme customizer */

( function( $ ){
	wp.customize('blogname',function( value ) {
		value.bind(function(to) {
			$('#site-title a').html(to);
		});
	});
	wp.customize('blogdescription',function( value ) {
		value.bind(function(to) {
			$('#site-description').html(to);
		});
	});
	wp.customize('printing_shop_color_scheme',function( value ) {
		value.bind( function( to ) {
			var classes = $( 'body' ).attr( 'class' ).replace( /color-[a-zA-Z0-9_-]*/g, '' );
			$( 'body' ).attr( 'class', classes ).addClass( 'color-' + to );
			$( '.default-logo' ).attr('src', ps_js_options.logos[to] );
			//$( '.post-thumbnail' ).attr('src', ps_js_options.features[to] );
			$( '.post-thumbnail' ).each(function() {
				var imgsrc = $(this).attr('src');
				if ( imgsrc.indexOf('featured-image-') != -1 ) {
					$(this).attr('src', ps_js_options.features[to] );
				}
			});
		});
	});
	wp.customize('printing_shop_woo_cart_icon', function( value ) {
		value.bind( function( to ) {
			var classes = $( '#header-shopping-cart' ).attr( 'class' ).replace( /icon-[a-zA-Z0-9_-]*/g, '' );
			$( '#header-shopping-cart' ).attr( 'class', classes ).addClass( 'icon-' + to );
		});
	});
})( jQuery )