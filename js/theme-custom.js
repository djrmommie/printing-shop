/**
 * printing shop js
 */

(function ($) {
	"use strict";
	$(function () {
		/* Mobile menu. */
		$( '.menu-toggle' ).click(
			function() {
				$( this ).parent().children( '.wrap, .menu-items' ).fadeToggle();
				$( this ).toggleClass( 'active' );
			}
		);

		/* Responsive videos. */
		$( '.format-video object, .format-video embed, .format-video iframe' ).removeAttr( 'width height' ).wrap( '<div class="embed-wrap" />' );

		$(document).ready(function() {
			if ( $.isFunction( $.fn.flexslider ) ) {
		          	$('.flexslider').flexslider({
		          		animation: "slide",
		          		controlNav: false
		          	});
			}
			
			
			if ( $.isFunction( $.fn.magnificPopup ) ) {
				$('.gallery-icon').magnificPopup({
					delegate: 'a', // child items selector, by clicking on it popup will open
					type: 'image',
					gallery: {
						enabled: true, // set to true to enable gallery
						preload: [0,2], // read about this option in next Lazy-loading section
						navigateByImgClick: true
					}
				});
			}

	    });

	    $(window).resize( function () {
			$('.ps_box_widget .box-content').each(function() {
				hideBoxImage( $(this) );
			});
			$('.products a').each(function() {
				setBoxHeight( $(this) );
			});
	    }).resize();

		function hideBoxImage( item ) {
			var icon = item.find("a.ps-icon-box");
			if ( !icon.hasClass("ps-hide-icon") && icon.height() > 130 ) {
				item.find("a").addClass( "ps-hide-icon" );
			} else if ( icon.hasClass("ps-hide-icon") && item.height() < 129 ) {
				icon.addClass( "ps-check-icon" );
				if ( icon.height() > 130 ) {
					icon.removeClass( "ps-check-icon" );
				} else {
					item.find("a").removeClass( "ps-hide-icon" );
					icon.removeClass( "ps-check-icon" );
				}				
			}
		}

		function setBoxHeight( item ) {
			var link_height = item.width();
			var h3_height = item.children("h3:first").height();
			var nh = link_height + h3_height;
			item.height(nh);
		}

	});
}(jQuery));