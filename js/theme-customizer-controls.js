/* javascript for theme customizer options panel */

( function( $ ){
	$('#customize-control-printing_shop_logo_type input[name="_customize-radio-printing_shop_logo_type"]').change( function() {
		var option_sel = $('input[name="_customize-radio-printing_shop_logo_type"]:checked').val();
		if ( option_sel == 'custom' ) {
			$('#customize-control-printing_shop_logo').show();
		} else {
			$('#customize-control-printing_shop_logo').hide();
		}
	}).change();
})( jQuery )