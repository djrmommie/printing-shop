<?php if ( is_active_sidebar( 'home-page-three' ) ) { ?>

	<section id="sidebar-home-three" class="sidebar">

		<?php dynamic_sidebar( 'home-page-three' ); ?>

	</section><!-- #sidebar-home-three -->

<?php } elseif ( is_front_page() && !is_home() ) { // only show hot off the press for static front page ?>

	<section id="sidebar-home-three" class="sidebar">

		<?php the_widget(
			'Printing_Shop_Post_Widget',
			array(
				'title'              => esc_attr__( 'Hot Off the Press', 'printing-shop', 'Front page default widget title' ),
				'taxonomy'           => 'category',
				'order'              => 'DESC',
				'orderby'            => 'date',
				'limit'             => '3',
				),
			array(
				'before_widget' => sprintf('<div id="%1$s" class="widget %2$s widget-%2$s"><div class="widget-wrap widget-inside">', 'ps-posts', 'hot_off_press_widget'),
				'after_widget'  => '</div></div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
				)
			);

		wp_reset_query(); ?>

	</section><!-- #sidebar-home-three -->

<?php } // endif is_active_sidebar ?>