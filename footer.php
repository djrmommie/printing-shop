				<?php get_sidebar( 'primary' ); // Loads the sidebar-primary.php template. ?>

			</div><!-- #main -->

		</div><!-- .wrap -->

	</div><!-- #container -->

		<?php $footer_callout = get_theme_mod( 'printing_shop_footer_callout', printing_shop_default_footer_callout() ); ?>
		<?php $footer_content = apply_atomic_shortcode( 'footer_content', hybrid_get_setting( 'footer_insert' ) ); ?>
	
		<footer id="footer">

			<div class="wrap <?php if ( $footer_callout ) echo 'has-footer-callout'; ?> <?php if ( $footer_content ) echo 'has-footer-content'; ?>">

				<?php get_sidebar( 'subsidiary' ); // Loads the sidebar-subsidiary.php template. ?>

				<div class="wrap">

					<div id="footer-left">

						<?php get_sidebar( 'footer' ); // Loads the sidebar-footer.php template. ?>

						<div class="footer-content">
							<?php echo $footer_content; ?>
						</div><!-- .footer-content -->

					</div>

					<div id="footer-right">

						<?php get_sidebar( 'footer-right' ); // Loads the sidebar-footer-right.php template. ?>

						<?php if ( $footer_callout ) { ?>

							<div id="footer-callout">
								<div class="wrap">
									<?php echo get_theme_mod( 'printing_shop_footer_callout', printing_shop_default_footer_callout() ); ?>
								</div>
							</div>

						<?php } ?>

					</div>

				</div>

			</div>

		</footer><!-- #footer -->

	<?php wp_footer(); // wp_footer ?>

</body>
</html>