<?php if ( is_active_sidebar( 'footer-right-widgets' ) ) { ?>

	<div id="sidebar-footer-right" class="sidebar">

		<?php dynamic_sidebar( 'footer-right-widgets' ); ?>

	</div><!-- #sidebar-footer .aside -->

<?php } elseif ( has_nav_menu( 'social' ) ) { ?>

	<div id="sidebar-footer-right" class="sidebar">
		<aside class="widget nav-menu widget-nav-menu">
			<div class="widget-wrap widget-inside">
			<h3 class="widget-title"><?php esc_html_e("we're social", 'printing-shop'); ?></h3>
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'social',
						'container'       => 'nav',
						'container_id'       => 'social-menu-default',
						'container_class' => 'menu-social-menu-container',
						'menu_id'         => 'menu-social-menu',
						'menu_class'      => 'social-menu',
						'fallback_cb'     => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>'
					)
				); ?>
			</div><!-- .widget-wrap -->
		</aside><!-- .widget -->
	</div><!-- #sidebar-footer .aside -->

<?php }