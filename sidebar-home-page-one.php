<?php $slider_setting = intval( get_theme_mod( 'printing_shop_slider', 0 ) ); ?>

<?php if ( is_active_sidebar( 'home-page-one' ) ) { ?>

	<section id="sidebar-home-one" class="sidebar">

		<?php dynamic_sidebar( 'home-page-one' ); ?>

	</section><!-- #sidebar-home-one -->

<?php } elseif ( 999999 != $slider_setting )  { // 999999 is our no slider setting ?>

	<section id="sidebar-home-one" class="sidebar">

		<div id="home-flexslider" class="widget">
			<div class="widget-wrap widget-inside">
			
			<?php if ( $slider_setting && function_exists( 'soliloquy_slider' ) ) {

				/** use soliloquy **/
				soliloquy_slider( intval(get_theme_mod( 'printing_shop_slider' )) );

			} else {

				/** use default flexslider **/
				$slides = printing_shop_slideshow();

				if ( $slides->have_posts() ) { ?>
					<div class="flexslider">
						<ul class="slides">
						<?php while ( $slides->have_posts() ) {
							$slides->the_post();
							if ( has_post_thumbnail() ) { ?>
								<li>
									<div class="ps-slide-content">
										<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php the_post_thumbnail( 'ps-home-slider' ); ?></a>
										<div class="ps-slider-meta">											
											<h3><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php the_title(); ?></a></h3>
											<div><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php _e( 'Read More', 'printing-shop' ); ?></a></div>
										</div>
									</div>
								</li>
							<?php  } // end has_post_thumbnail

							printing_shop_enqueue_flexslider(); // enqueue the jquery for flexslider

						} // end while have_posts ?>
						</ul>
					</div>
				<?php } 
				
				wp_reset_postdata(); //restore original post data 
				
			} ?>
			</div><!-- .widget-wrap-->
		</div><!-- #home-slider -->

	</section><!-- #sidebar-home-one -->

<?php } // endif is_active_sidebar ?>