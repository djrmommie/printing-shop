<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
<title><?php hybrid_document_title(); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); // wp_head ?>

</head>

<body <?php hybrid_body_attributes(); ?>>

	<div id="container">

		<div class="wrap">

			<header id="header">

				<div id="branding">
					<h1 id="site-title">
						<a href="<?php echo home_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
						<?php if ( 'text' == get_theme_mod( 'printing_shop_logo_type' ) ) : ?>
							<?php bloginfo( 'name' ); ?>
						<?php elseif ( 'custom' == get_theme_mod( 'printing_shop_logo_type' ) && get_theme_mod( 'printing_shop_logo' ) ) : ?>
							<img class="logo" src="<?php echo esc_url( get_theme_mod( 'printing_shop_logo' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
						<?php else: ?>
							<img class="logo default-logo" src="<?php echo esc_url( printing_shop_default_logo() ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
						<?php endif; ?>
						</a>
					</h1>					
					<?php if ( 'text' == get_theme_mod( 'printing_shop_logo_type' ) ) : ?>
						<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
					<?php endif; ?>
				</div><!-- #branding -->

				<?php get_sidebar( 'header' ); // Loads the sidebar-header.php template. ?>

			</header><!-- #header -->

			<?php get_template_part( 'menu', 'primary' ); // Loads the menu-primary.php template. ?>

			<div id="main">