<?php get_header(); // Loads the header.php template. ?>

	<div id="content" class="hfeed">

		<?php get_sidebar( 'home-page-one' ); // Loads the sidebar-home-page-one.php template. ?>

		<?php get_sidebar( 'home-page-two' ); // Loads the sidebar-home-page-two.php template. ?>

		<?php get_sidebar( 'home-page-three' ); // Loads the sidebar-home-page-three.php template. ?>

	</div><!-- #content -->

<?php get_footer(); // Loads the footer.php template. ?>