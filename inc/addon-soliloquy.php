<?php
/**
 * Solilouqy Support
 *
 * @package PrintingShop
 * @subpackage Includes
 * @since 1.0.0
 */

add_filter( 'soliloquy_output_after_link', 'printing_shop_output_after_link', 10, 5 );
/**
 * Outputs opening of ps-slider-meta div and link and title if available
 *
 * @since 1.1.0
 * @param $output
 * @param $id
 * @param $item
 * @param $data
 * @param $i
 *
 * @return string
 */
function printing_shop_output_after_link( $output, $id, $item, $data, $i ) {

	if ( isset( $data['id'] ) && $data['id'] == intval(get_theme_mod( 'printing_shop_slider' )) ) { // only apply to the choosen home page slider
		$output .= '<div class="ps-slider-meta">';
		if ( ! empty( $item['link'] ) ) {
			$output  = apply_filters( 'soliloquy_output_before_link', $output, $id, $item, $data, $i );
			$output .= '<a href="' . esc_url( $item['link'] ) . '" class="soliloquy-link" title="' . esc_attr( $item['title'] ) . '"' . apply_filters( 'soliloquy_output_link_attr', '', $id, $item, $data, $i ) . '>';
		}
		if ( ! empty( $item['title'] ) ) {
			$output .= '<h3>' . esc_attr( $item['title'] ) . '</h3>';
		}
	}

	return $output;
}

add_filter( 'soliloquy_output_image_slide', 'printing_shop_output_image_slide', 10, 3 );
/**
 * Outputs closing link and div for ps-slide-meta
 *
 * @since 1.1.0
 * @param $output
 * @param $id
 * @param $item
 *
 * @return string
 */
function printing_shop_output_image_slide( $output, $id, $item ) {

	if ( $id == intval(get_theme_mod( 'printing_shop_slider' )) ) { // only apply to the choosen home page slider
		if ( ! empty( $item['link'] ) ) {
			$output .= '</a>';
		}
		$output .= '</div>';
	}

	return $output;
}

add_filter( 'soliloquy_pre_data', 'printing_shop_pre_data', 10, 2 );
/**
 * Returns slider width and height for cropping to set home page slider design
 * @since 1.1.0
 * @param $data
 * @param $slider_id
 *
 * @return array
 */
function printing_shop_pre_data( $data, $slider_id ) {

	if ( $slider_id == intval( get_theme_mod( 'printing_shop_slider' ) ) ) { // only apply to the choosen home page slider
		$data['config']['slider_width'] = 733;
		$data['config']['slider_height'] = 405;
	}

	return $data;
}

add_filter( 'soliloquy_slider_themes', 'printing_shop_soliloquy_themes' );
/**
 * @since  1.1.0
 * Adds custom theme to soliloquy
 * @param $themes
 * @return array with theme added
 */
function printing_shop_soliloquy_themes( $themes ) {
	$themes[] =
		array(
			'value' => 'printing-shop',
			'name'  => __( 'Printing Shop', 'soliloquy' ),
			'file'  => '' // this is useless since we aren't putting our theme in the plugins theme folder
		);

	return $themes;
}

add_action( 'wp_footer', 'printing_shop_remove_theme_css', 1 );
/**
 * @since  1.1.0
 * hacky solution to keep soliloquy from enqueuing theme style since it is in our theme css already,
 * using soliloquy_before_output hook simple because it is the first hook after soliloquy
 * does the enqueue
 *
 */
function printing_shop_remove_theme_css() {
	// this is pretty hacky since we can't know for sure the plugin name
	wp_dequeue_style( 'soliloquy-liteprinting-shop-theme' );
	wp_dequeue_style( 'soliloquyprinting-shop-theme' );
}
