<?php
/**
 * Theme Functions
 *
 * @package PrintingShop
 * @subpackage Includes
 * @since 1.0.0
 */

/**
 * Function for deciding which pages should have a one-column layout.
 *
 * @since 1.0.0
 * @access public
 * @return void
 */
function printing_shop_theme_layout() {

	if ( !is_active_sidebar( 'primary' ) )
		add_filter( 'theme_mod_theme_layout', 'printing_shop_theme_layout_one_column' );

	elseif ( is_attachment() && wp_attachment_is_image() && 'default' == get_post_layout( get_queried_object_id() ) )
		add_filter( 'theme_mod_theme_layout', 'printing_shop_theme_layout_one_column' );
}

/**
 * Filters 'get_theme_layout' by returning 'layout-1c'.
 *
 * @since 1.0.0
 * @access public
 * @param string $layout The layout of the current page.
 * @return string
 */
function printing_shop_theme_layout_one_column( $layout ) {
	return '1c';
}

/**
 * register nav menu for social icons
 * @since  1.0.0
 * @return void
 */
function printing_shop_register_menus() {

	register_nav_menu( 'social', esc_html__( 'Social Links', 'printing-shop' ) );

}

/**
 * Disables sidebars if viewing a one-column page.
 *
 * @since 1.0.0
 * @access public
 * @param array $sidebars_widgets A multidimensional array of sidebars and widgets.
 * @return array $sidebars_widgets
 */
function printing_shop_disable_sidebars( $sidebars_widgets ) {

	if ( current_theme_supports( 'theme-layouts' ) && !is_admin() ) {

		if ( 'layout-1c' == theme_layouts_get_layout() ) {
			$sidebars_widgets['primary'] = false;
		}
	}

	return $sidebars_widgets;
}

/**
 * Registers and loads the theme's scripts.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function printing_shop_enqueue_scripts() {

	/* Enqueue the printing-shop.js script. */
	wp_enqueue_script( 'printing-shop', hybrid_locate_theme_file( 'js/theme-custom.js' ), array( 'jquery' ), '20130918', true );

		/* Load the comment reply script on singular posts with open comments if threaded comments are supported. */
	if ( is_singular() && get_option( 'thread_comments' ) && comments_open() && printing_shop_comments_display() )
		wp_enqueue_script( 'comment-reply' );

	/* remove feed links if hiding comments */
	if ( is_singular() && ! printing_shop_comments_display() )
		remove_action( 'wp_head', 'feed_links_extra', 3 );
}

/**
 * enqueue flexslider
 * @since  1.0.0
 * @return void
 */
function printing_shop_enqueue_flexslider() {

	/* Enqueue the flexslider.js script. */
	wp_enqueue_script( 'printing-shop-flexslider', hybrid_locate_theme_file( 'js/flexslider/jquery.flexslider-min.js' ), array( 'jquery' ), '2.2.0', true );
}

function printing_shop_gallery( $output, $attr ) {

	if ( isset( $attr['link'] ) && 'file' === $attr['link'] ) {
		printing_shop_enqueue_magnific();
	}

	return $output; // not actually changed
}

/**
 * enqueue magnific popup
 * @since  1.0.0
 * @return void
 */
function printing_shop_enqueue_magnific() {

	/* Enqueue the printing-shop.js script. */
	wp_enqueue_script( 'magnific-popup', hybrid_locate_theme_file( 'js/magnific/magnific.min.js' ), array( 'jquery' ), '0.9.9', true );
}

/**
 * Function for help to unsupported browsers understand mediaqueries and html5.
 * @link: https://github.com/scottjehl/Respond
 * @link: http://code.google.com/p/html5shiv/
 * @since 1.0.0
 */
function printing_shop_respond_html5shiv() {
	?>
	
	<!-- Enables media queries and html5 in some unsupported browsers. -->
	<!--[if (lt IE 9) & (!IEMobile)]>
	<script type="text/javascript" src="<?php echo trailingslashit( get_template_directory_uri() ); ?>js/respond/respond.min.js"></script>
	<script type="text/javascript" src="<?php echo trailingslashit( get_template_directory_uri() ); ?>js/html5shiv/html5shiv.js"></script>
	<![endif]-->
	
	<?php
}

/**
 * Add additional widget areas for home page template
 * 
 * @since 1.0.0
 * @access public
 * @return void
 */
function printing_shop_register_sidebars() {

	register_sidebar(
		array(
				'name'        => _x( 'Header', 'sidebar', 'printing-shop' ),
				'description' => __( 'Header widget area, displays to right of logo', 'printing-shop' ),
				'id' => 'header-widgets',
				'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
				'after_widget'  => '</div></aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
			));

	register_sidebar(
		array(
				'name'        => _x( 'Footer', 'sidebar', 'printing-shop' ),
				'description' => __( 'Footer widget area', 'printing-shop' ),
				'id' => 'footer-widgets',
				'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
				'after_widget'  => '</div></aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
			));

	register_sidebar(
		array(
			'name'        => _x( 'Footer Right', 'sidebar', 'printing-shop' ),
			'description' => __( 'Right side footer widget area', 'printing-shop' ),
			'id' => 'footer-right-widgets',
			'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
			'after_widget'  => '</div></aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		));

	register_sidebar(
		array(
		'name'        => _x( 'Home Page Top', 'sidebar', 'printing-shop' ),
		'description' => __( 'Home Page Template, top widget area.  Add widgets here to replace the slideshow with your own choice of widgets.', 'printing-shop' ),
		'id' => 'home-page-one',
		'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	));

	register_sidebar(
		array(
			'name'        => _x( 'Home Page Middle', 'sidebar', 'printing-shop' ),
			'description' => __( 'Home Page Template, middle widget area.  ', 'printing-shop' ),
			'id' => 'home-page-two',
			'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		));

	register_sidebar(
		array(
			'name'        => _x( 'Home Page Bottom', 'sidebar', 'printing-shop' ),
			'description' => __( 'Home Page Template, bottom widget area.  Add widgets here to replace the default From the Blog content.', 'printing-shop' ),
			'id' => 'home-page-three',
			'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		));
}

/**
 * Removes breadcrumb trail
 * 
 * @since  1.0.0
 * @access public
 * @return void
 */
function printing_shop_breadcrumb_trail( $breadcrumb ) {

	if ( is_home() || is_front_page() )
		$breadcrumb = '';

	return $breadcrumb;
}

/**
 * Registers custom image sizes for the theme.  
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function printing_shop_register_image_sizes() {

	/* Size: 'post-thumbnail' */
	set_post_thumbnail_size( 250, 135, true );

	/* additional image sizes */
	add_image_size( 'ps-home-slider', 780, 405, true );
	add_image_size( 'ps-feature-1c', 1000, 240, true );
	add_image_size( 'ps-feature-2c', 750, 240, true );

}

/**
 * Loads extra widget files and registers the widgets.
 * 
 * @since  1.0.0
 * @access public
 * @return void
 */
function printing_shop_register_widgets() {

	/* Load and register the posts widget. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget-posts.php' );
	register_widget( 'Printing_Shop_Post_Widget' );

	/* Load and register the box widget. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget-boxes.php' );
	register_widget( 'Printing_Shop_Box_Widget' );

	/* Load and register the small box widget. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget-boxes-small.php' );
	register_widget( 'Printing_Shop_Box_Small_Widget' );
}
/**
 * Filters the excerpt length.
 *
 * @since 1.0.0
 */
function printing_shop_excerpt_length( $length ) {
	return 55;
}


/**
 * Filters the excerpt more.
 *
 * @since 1.0.0
 */

function printing_shop_new_excerpt_more( $more ) {
	return ' <span class="ps-read-more"><a class="more-link" href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '">  ' . __( 'Read More', 'printing-shop' ) . ' </a></span>';
}

/**
 * adds class for color/skin option
 *
 * @since 1.0.0
 * @param  array $classes
 * @return array
 */
function printing_shop_custom_body_classes( $classes ) {

	if ( is_active_sidebar( 'header-widgets' ) || class_exists( 'woocommerce' ) ) {
		$classes[] = "header-widget";
	}

	if ( $color_skin = get_theme_mod( 'printing_shop_color_scheme', 'bluematte' ) ) {
		$classes[] = "color-$color_skin";
	}
	return $classes;
}

/**
 * adds body class to tinymce editor so the "skin" colors will be applied in editor-styles
 *
 * @notes credit to Slobodan Manic http://wp.tutsplus.com/tutorials/creative-coding/using-google-fonts-in-editor-style/
 * 
 * @since  1.0.0	
 * @param  array    $thsp_mceInit array of tinymce settings
 * @return array    return settings array with body class added
 */
function printing_shop_tiny_mce_classes( $thsp_mceInit ) {
    
    if ( $color_skin = get_theme_mod( 'printing_shop_color_scheme', 'bluematte' ) ) {
		$thsp_mceInit['body_class'] .= " color-$color_skin";
	}
	return $thsp_mceInit;
}

/**
 * wp_query for slideshow, gets most recent 5 posts that have a featured image
 * @since 1.0.0
 * @return object wp_query object
 */
function printing_shop_slideshow() {

	$slides = new WP_Query( 
		array(
			'posts_per_page' => 5,
			'meta_query' => array(
				array(
					'key' => '_thumbnail_id',
					'compare' => 'EXISTS'
				)
			)
		)
	);

	return $slides;
}

/**
 * Adds a custom stylesheet to the Hybrid styles loader.  We need to do this so that it's loaded in the correct 
 * order (before the theme style).
 *
 * @since  1.0.0
 * @access public
 * @param  array  $styles
 * @return array
 */
function printing_shop_styles( $styles ) {

	$styles['printing-shop-fontawesome'] = array(
		'version' => '4.2.0',
		'src'     => hybrid_locate_theme_file( 'css/font-awesome.css' )
	);

	return $styles;
}

/**
 * Adds a widget-wrap div to hybrid core sidebars
 * @since  1.0.0	
 * @param  array   $defaults hybrid widget defaults
 * @return array
 */
function printing_shop_sidebar_defaults( $defaults ) {

	$defaults['before_widget'] = '<section id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">';
	$defaults['after_widget']  = '</div></section>';

	return $defaults;
}

/**
 * Sets front-page.php to only be used on front page as home page
 * @since  1.0.0
 * @param  string    $template 
 * @return string
 */
function printing_shop_front_page_template( $template ) {
    return is_home() ? '' : $template;
}

/**
 * returns true if this is the front page and blog index and the first page of blog index
 * @since  1.0.0
 * @return bool    true or false
 */
function printing_shop_is_blog_home() {
	global $page, $paged;
	if ( is_front_page() && !( $paged >= 2 || $page >= 2 ) )
		return true;
	else
		return false;
}

function printing_shop_get_featured_img( $post_id) {

	if ( current_theme_supports( 'theme-layouts' ) ) {

		if ( 'layout-1c' == theme_layouts_get_layout() ) {
			$size = 'ps-feature-1c'; // for single column layout
		} else {
			$size = 'ps-feature-2c'; // for 2 column layout
		}
	} else {
		$size = 'large'; // default to large image
	}

	return get_the_post_thumbnail( $post_id, $size );
}