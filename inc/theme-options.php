<?php
/**
 * Functions, hooks, and filters related to theme options
 *
 * @package PrintingShop
 * @subpackage Includes
 * @since 1.0.0
 */

/* Register custom sections, settings, and controls */
add_action( 'customize_register', 'printing_shop_customize_register', 11 );

/* Load custom control classes. */
add_action( 'customize_register', 'printing_shop_load_customize_controls', 1 );

/* Load customer preview js */
add_action( 'customize_preview_init', 'printing_shop_customize_preview', 21 );

/* register admin widget styles */
add_action( 'customize_controls_init', 'printing_shop_admin_register_styles' );

/* Load customer panel js */
add_action( 'customize_controls_enqueue_scripts', 'printing_shop_customize_options', 21 );

/** Hide the hybrid theme settings page **/
add_action( 'wp_loaded', 'printing_shop_remove_settings_menu', 11);

/* add favicon */
add_action( 'wp_head', 'printing_shop_favicon' );

/* Add the admin setup function to the 'admin_menu' hook. */
add_action( 'admin_menu', 'printing_shop_admin_setup' );

/**
 * Add theme customizer sections, settings, and controls.
 *
 * @since 1.0.0
 */
function printing_shop_customize_register( $wp_customize ) {
	
	/* Logo Upload */

	$wp_customize->add_section(
		'printing_shop_logo_settings',
		array(
			'title' => __( 'Logo', 'printing-shop' ),
			'description' => __( 'Settings for logo and title.', 'printing-shop'),
			'priority' => 35
		)
	);

	$wp_customize->add_setting(
		'printing_shop_logo_type',
		array(
			'default'           => '',
			'sanitize_callback' => 'esc_attr',
			//'transport'         => 'postMessage'
		)
	);
	
	/* Add color scheme control. */
	$wp_customize->add_control(
		'printing_shop_logo_type',
		array(
			'label'    => esc_html__( 'Logo Options', 'printing-shop' ),
			'section'  => 'printing_shop_logo_settings',
			'settings' => 'printing_shop_logo_type',
			'type'     => 'radio',
			'priority' => 1,
			'choices'  => array(
				''       => esc_html__( 'Default Logo', 'printing-shop' ),
				'text'       => esc_html__( 'Text Only (Site Title &amp; Description)', 'printing-shop' ),
				'custom'       => esc_html__( 'Upload Custom', 'printing-shop' )
			)
		)
	);

	$wp_customize->add_setting(
		'printing_shop_logo',
		array(
			'default' => '',
			'sanitize_callback' => 'esc_url_raw'
		)
	);

	$wp_customize->add_control(
		new My_Customize_Image_Control(
			$wp_customize,
			'printing_shop_logo',
			array(
				'label' => __( 'Logo Upload', 'printing-shop' ),
				'section' => 'printing_shop_logo_settings',
				'settings' => 'printing_shop_logo'
			)
		)
	);

	/* Add color scheme setting. */
	$wp_customize->add_setting(
		'printing_shop_color_scheme',
		array(
			'default'           => 'bluematte',
			'sanitize_callback' => 'esc_attr',
			'transport'         => 'postMessage'
		)
	);
	
	/* Add color scheme control. */
	$wp_customize->add_control(
		'printing_shop_color_scheme',
		array(
			'label'    => esc_html__( 'Choose Color Scheme', 'printing-shop' ),
			'section'  => 'colors',
			'settings' => 'printing_shop_color_scheme',
			'type'     => 'radio',
			'priority' => 1,
			'choices'  => array(
				'bluematte'       => esc_html__( 'Blue Matte', 'printing-shop' ),
				'candy'       => esc_html__( 'Candy Gloss', 'printing-shop' ),
				'linen'       => esc_html__( 'Natural Linen', 'printing-shop' )
			)
		)
	);

	// home page section
	$wp_customize->add_section(
		'printing_shop_home_settings',
		array(
			'title' => __( 'Home Page', 'printing-shop' ),
			'description' => __( 'Settings for the home page.', 'printing-shop'),
			'priority' => 25
		)
	);

	/** credit to Sami Keijonen https://foxnet-themes.fi/ **/
	$printing_shop_soliloquy_slider_choices = printing_shop_get_soliloquy_slider_choices();
	
	$wp_customize->add_setting(
		'printing_shop_slider',
		array(
			'default'           => '0',
			'sanitize_callback' => 'absint'
		)
	);
	
	/* Labels for Soliloquy slider control. */
	if ( post_type_exists( 'soliloquy' ) ) {
		$printing_shop_soliloquy_slider_control_label = esc_html__( 'Select Slider', 'printing-shop' );
	} else {
		$printing_shop_soliloquy_slider_control_label = esc_html__( 'Install and activate Soliloquy Slider Plugin to enable additional slider options.', 'printing-shop' );
	}
	
	/* Add the Soliloquy Slider control. */
	$wp_customize->add_control(
		'printing_shop_slider',
		array(
			'label'    => $printing_shop_soliloquy_slider_control_label,
			'section'  => 'printing_shop_home_settings',
			'type'     => 'select',
			'choices'  => $printing_shop_soliloquy_slider_choices
		)
	);

	// favicon section

	$wp_customize->add_section(
		'printing_shop_favicon_settings',
		array(
			'title' => __( 'Favicon', 'printing-shop' ),
			'description' => __( 'Remove the the default favicon', 'printing-shop'),
			'priority' => 35
		)
	);

	$wp_customize->add_setting(
		'printing_shop_disable_favicon',
		array(
			'default' => 0,
			'sanitize_callback' => 'absint'
		)
	);

	$wp_customize->add_control(
		'printing_shop_disable_favicon',
		array(
			'label'   => __( 'Remove favicon', 'printing-shop' ),
			'section' => 'printing_shop_favicon_settings',
			'type'    => 'checkbox',
		)
	);

	// footer 	
	$wp_customize->add_setting(
		'printing_shop_footer_callout',
		array(
			'default'              => printing_shop_default_footer_callout(),
			'sanitize_callback'    => 'wp_filter_post_kses'
		)
	);

	$wp_customize->add_control(
		new Hybrid_Customize_Control_Textarea(
			$wp_customize,
			'printing_shop_footer_callout',
			array(
				'label'    => esc_html__( 'Print Shop Hours', 'printing-shop' ),
				'section'  => 'hybrid-core-footer',
				'settings' => 'printing_shop_footer_callout',
			)
		)
	);

	// comments display section

	$wp_customize->add_section(
		'printing_shop_comment_settings',
		array(
			'title' => __( 'Comments', 'printing-shop' ),
			'description' => __( 'Display or hide Coments', 'printing-shop'),
			'priority' => 85
		)
	);

	$wp_customize->add_setting(
		'printing_shop_page_comment_display',
		array(
			'default' => 1,
			'sanitize_callback' => 'absint'
		)
	);

	$wp_customize->add_setting(
		'printing_shop_post_comment_display',
		array(
			'default' => 1,
			'sanitize_callback' => 'absint'
		)
	);

	$wp_customize->add_control(
		'printing_shop_post_comment_display',
		array(
			'label'   => __( 'Display comments, trackbacks, & pingbacks on Posts', 'printing-shop' ),
			'section' => 'printing_shop_comment_settings',
			'type'    => 'checkbox',
		)
	);

	$wp_customize->add_control(
		'printing_shop_page_comment_display',
		array(
			'label'   => __( 'Display comments, trackbacks, & pingbacks on Pages', 'printing-shop' ),
			'section' => 'printing_shop_comment_settings',
			'type'    => 'checkbox',
		)
	);

	// woocommerce
	$wp_customize->add_section(
		'printing_shop_woo_settings',
		array(
			'title' => __( 'WooCommerce', 'printing-shop' ),
			'description' => __( 'Theme settings for wooComerce', 'printing-shop'),
			'priority' => 95
		)
	);

	$wp_customize->add_setting(
		'printing_shop_woo_cart_icon',
		array(
			'default' => 'printer',
			'sanitize_callback'    => 'esc_attr'
		)
	);

	/* Labels for printing_shop_woo_cart_icon */
	if ( class_exists('Woocommerce') ) {
		$printing_shop_woocart_icon_control_label = esc_html__( 'Select Mini-Cart Icon Style', 'printing-shop' );
	} else {
		$printing_shop_woocart_icon_control_label = esc_html__( 'Install and activate Woocommerce to use this feature', 'printing-shop' );
	}

	$wp_customize->add_control(
		'printing_shop_woo_cart_icon',
		array(
			'label'    => $printing_shop_woocart_icon_control_label,
			'section'  => 'printing_shop_woo_settings',
			'type'     => 'select',
			'choices'  => array(
					'printer'	 	=> __( 'Ready to Print', 'printing-shop' ),
					'cart'	=> __( 'In Your Cart', 'printing-shop' )
				)
		)
	);

	$wp_customize->get_setting('blogname')->transport='postMessage';
	$wp_customize->get_setting('blogdescription')->transport='postMessage';
}

/**
 * load js theme preview panel
 * 
 * @since  1.0.0
 */
function printing_shop_customize_preview() {
	
	wp_enqueue_script( 
		'printing-shop-themecustomizer', 
		trailingslashit( get_template_directory_uri() ) . 'js/theme-customizer.js',
		array( 'jquery','customize-preview' ), 
			'1.0.0', 
			true 
	); 
	wp_localize_script(
		'printing-shop-themecustomizer',
		'ps_js_options',
		array(
				'logos'    => printing_shop_logo_array(),
				'features' => printing_shop_featured_images(),
				'ajaxurl'  => get_admin_url( 'admin-ajax.php' ),
				'nonce'	   => wp_create_nonce( 'hybrid_customize_footer_content_nonce' ),
			)
	);
} 

/**
 * load js and css for theme customizer options panel
 * 
 * @since  1.0.0
 */
function printing_shop_customize_options() {
	wp_enqueue_style( 'printing-shop-admin-widgets' );
	wp_enqueue_script( 
		'printing-shop-themecustomizer', 
		trailingslashit( get_template_directory_uri() ) . 'js/theme-customizer-controls.js',
		array( 'jquery' ), 
			'1.0.0', 
			true 
	); 
} 

/**
 * Load custom controls.
 *
 * @since 1.0.0
 */

function printing_shop_load_customize_controls() {

	/* Loads the textarea customize control class. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/customize-control-image-upload-reloaded.php' );
}

/**
 * Remove the hook that adds the hybrid theme setings page while still using 
 * footer in customizer  fromhybrid-core-theme-settings
 * 
 * @since 1.0.0
 * @access public
 * @return void
 */
function printing_shop_remove_settings_menu() {
	remove_action( 'admin_menu', 'hybrid_settings_page_init' );
}

/**
* Return Soliloquy Slider choices.
*
* @since 1.0.0
* @return array slider options from soliloquy post type
*/
function printing_shop_get_soliloquy_slider_choices() {
	
	/* Set an array. */
	$printing_shop_slider_data = array(
		'0' => __( 'Default - Featured Images', 'printing-shop' )
	);

	/* only if soliloquy active */
	if ( post_type_exists( 'soliloquy' ) ) {
			
		/* Get Soliloquy Sliders. */
		$printing_shop_soliloquy_args = array(
			'post_type' 		=> 'soliloquy',
			'posts_per_page' 	=> -1
		);
		
		$printing_shop_sliders = get_posts( $printing_shop_soliloquy_args );
		
		/* Loop sliders data. */
		foreach ( $printing_shop_sliders as $printing_shop_slider ) {
			$printing_shop_slider_data[$printing_shop_slider->ID] = $printing_shop_slider->post_title;
		}

	}

	$printing_shop_slider_data[999999] =  __( 'Remove Slider', 'printing-shop' ); // choosing 999999 as unlikely ;)
	
	/* Return array. */
	return $printing_shop_slider_data;
	
}

/**
 * returns default location for printing_shop_logo setting
 *
 * @since 1.0.0
 * @return string
 */
function printing_shop_default_logo() {

	$logos = printing_shop_logo_array();
	$color = get_theme_mod( 'printing_shop_color_scheme', 'bluematte' );

	if ( $logos[$color] )
		return $logos[$color];
	else
		return get_template_directory_uri() . '/images/logo.png';

}

/**
 * logo options by color
 * 
 * @since  1.0.0
 * @return array   array of logo urls by color
 */
function printing_shop_logo_array() {

	return apply_filters( 'printing_shop_logo_array', array(
			'bluematte' => get_template_directory_uri() . '/images/logo-bluematte.png',
			'candy'     => get_template_directory_uri() . '/images/logo-candy.png',
			'linen'		=> get_template_directory_uri() . '/images/logo-linen.png',
		)
	);
}

/**
 * default featured image by color
 * @since  1.0.0			
 * @return array   featured image urls by color
 */
function printing_shop_featured_images() {

	return apply_filters( 'printing_shop_featured_images', array(
			'bluematte' => get_template_directory_uri() . '/images/featured-image-bluematte.png',
			'candy'     => get_template_directory_uri() . '/images/featured-image-candy.png',
			'linen'		=> get_template_directory_uri() . '/images/featured-image-linen.png',
		)
	);
}

/**
 * add favicon
 * 
 * @since  1.0.0
 * @return void 
 */
function printing_shop_favicon() {
	if ( get_theme_mod( 'printing_shop_disable_favicon' ) ) {
		// do nothing
	} else {
		$color_skin = get_theme_mod( 'printing_shop_color_scheme', 'bluematte' );
		$favicon = ( $favicon = hybrid_locate_theme_file( 'images/favicon-' . $color_skin . '.ico') ) ? $favicon : hybrid_locate_theme_file( 'images/favicon.ico');
			echo '<link rel="shortcut icon" href="' . esc_url( $favicon ) . '" />';
	}
}

/**
 * check whether the current template should show the featured image, should only be called from inside loop
 * 
 * @since  0.1.1
 * @return bool true or false
 */
function printing_shop_show_featured_image() {
	$check = false;

	if ( !hybrid_has_post_template( 'templates/post-featured-image.php' ) // this template shows alt featured image
		&& !hybrid_has_post_template( 'templates/post-no-featured-image.php' ) // this template never shows featured image
		&& !hybrid_has_post_template( 'templates/page-featured-image.php' ) // this template shows alt featured image
		&& !hybrid_has_post_template( 'templates/page-no-featured-image.php' ) ) // this template never shows featured image
	{

		$check = true;
	}

	return $check;
}

/**
 * generate default text for footer callout
 *
 * @since  1.0.0
 * @return string
 */
function printing_shop_default_footer_callout() {

	$default_text = '<p><strong>' . __( 'Hours of Operation', 'printing-shop' ) . '</strong><br />';
	$default_text .= "\n" . __( 'Sun: Closed', 'printing-shop' ) . '<br />';
	$default_text .= "\n" . __( 'Mon-Fri: 9am - 5pm', 'printing-shop' ) . '<br />';
	$default_text .= "\n" . __( 'Sat:10am- 3pm', 'printing-shop' ) . '</p>';

	return $default_text;
}

/**
 * check comment settings 
 *
 * @since  1.0.0
 * @param  string $post_type post type to check comment display option
 * @return bool true or false for option
 */
function printing_shop_comments_display( $post_type = false ) {
	// check first for woocommerce
	if ( class_exists( 'Woocommerce' ) ) {
		if ( is_cart() || is_checkout() || is_account_page() ) {
			return false;
		}
	}
	if ( !$post_type )
		$post_type = get_post_type(); // get post type from current post if not passed in
	if ( !$post_type )
		return false; // if still no post_type return false
	return get_theme_mod( 'printing_shop_' . $post_type . '_comment_display', 1 );
}

/**
 * Registers the admin css .
 *
 * @since 1.2.0
 * @return void
 */
function printing_shop_admin_register_styles() {

	wp_register_style( 'printing-shop-admin-widgets', hybrid_locate_theme_file( 'css/admin-widgets.css' ), false, '20130918', 'screen' );
}

/**
 * Sets up additional admin functionality
 *
 * @since 1.3.0
 * @return void
 */
function printing_shop_admin_setup() {

	/* Registers admin stylesheets for the framework. */
	add_action( 'admin_enqueue_scripts', 'printing_shop_admin_register_styles', 5 );

	/* Loads admin stylesheets for the framework. */
	add_action( 'admin_enqueue_scripts', 'printing_shop_enqueue_styles' );
}

/**
 * Loads the admin css
 *
 * @since 1.2.0
 * @return void
 */
function printing_shop_enqueue_styles( $hook_suffix ) {

	/* Load admin styles if on the widgets screen */
	if ( 'widgets.php' == $hook_suffix )
		wp_enqueue_style( 'printing-shop-admin-widgets' );
}

/**
 * Registers custom fonts for the Theme Fonts extension.
 *
 * @since  1.0.0
 * @access public
 * @param  object  $theme_fonts
 * @return void
 */
function printing_shop_register_fonts( $theme_fonts ) {

	/* Add the 'headlines' font setting. */
	$theme_fonts->add_setting(
		array(
			'id'        => 'headings',
			'label'     => __( 'Headings', 'printing-shop' ),
			'default'   => 'varela-round',
			'selectors' => 'h1, h2, h3, h4, h5, h6, #menu-primary, .heading-font, .whistles-tabs .whistles-tabs-nav li a, .whistles-toggle .whistle-title',
		)
	);

	/* Add the 'body' font setting. */
	$theme_fonts->add_setting(
		array(
			'id'        => 'body',
			'label'     => __( 'Body', 'printing-shop' ),
			'default'   => 'open-sans',
			'selectors' => 'body, .body-font, .whistles',
		)
	);

	/* Add fonts that users can select for this theme. */
	$theme_fonts->add_font(
		array(
			'handle' => 'varela-round',
			'label'  => __( 'Varela Round', 'printing-shop' ),
			'family' => 'Varela Round',
			'stack'  => 'Varela Round, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'headings'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'oldenburg',
			'label'  => __( 'Oldenburg', 'printing-shop' ),
			'family' => 'Oldenburg',
			'stack'  => 'Oldenburg, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'headings'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'delius-swash-caps',
			'label'  => __( 'Delius Swash Caps', 'printing-shop' ),
			'family' => 'Delius Swash Caps',
			'stack'  => 'Delius Swash Caps, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'headings'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'rum-raisin',
			'label'  => __( 'Rum Raisin', 'printing-shop' ),
			'family' => 'Rum Raisin',
			'stack'  => 'Rum Raisin, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'headings'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'roboto-condensed',
			'label'  => __( 'Roboto Condensed', 'printing-shop' ),
			'family' => 'Roboto Condensed',
			'stack'  => 'Roboto Condensed, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'headings'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'righteous',
			'label'  => __( 'Righteous', 'printing-shop' ),
			'family' => 'Righteous',
			'stack'  => 'Righteous, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'headings'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'walter-turncoat',
			'label'  => __( 'Walter Turncoat', 'printing-shop' ),
			'family' => 'Walter Turncoat',
			'stack'  => 'Walter Turncoat, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'headings'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'questrial',
			'label'  => __( 'Questrial', 'printing-shop' ),
			'family' => 'Questrial',
			'stack'  => 'Questrial, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'headings'
		)
	);

	/* Add fonts that users can select for this theme. */
	$theme_fonts->add_font(
		array(
			'handle' => 'open-sans',
			'label'  => __( 'Open Sans', 'printing-shop' ),
			'family' => 'Open Sans',
			'stack'  => 'Open Sans, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'news-cycle',
			'label'  => __( 'News Cycle', 'printing-shop' ),
			'family' => 'News Cycle',
			'stack'  => 'News Cycle, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'questrial',
			'label'  => __( 'Questrial', 'printing-shop' ),
			'family' => 'Questrial',
			'stack'  => 'Questrial, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'arimo',
			'label'  => __( 'Arimo', 'printing-shop' ),
			'family' => 'Arimo',
			'stack'  => 'Arimo, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'roboto',
			'label'  => __( 'Roboto', 'printing-shop' ),
			'family' => 'Roboto',
			'stack'  => 'Roboto, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'marvel',
			'label'  => __( 'Marvel', 'printing-shop' ),
			'family' => 'Marvel',
			'stack'  => 'Marvel, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);

	$theme_fonts->add_font(
		array(
			'handle' => 'quicksand',
			'label'  => __( 'Quicksand', 'printing-shop' ),
			'family' => 'Quicksand',
			'stack'  => 'Quicksand, Tahoma, Verdana, Segoe, sans-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);
}