<?php
/**
 * Box widget
 *
 * @package PrintingShop
 * @subpackage Includes
 * @since 1.0.0
 */

/**
 * Small Boxes Widget Class
 *
 * @since 1.0.0
 */
class Printing_Shop_Box_Small_Widget extends WP_Widget {

	/**
	 * Set up the widget's unique name, ID, class, description, and other options.
	 *
	 * @since 1.0.0
	 */
	function __construct() {

		/* Set up the widget options. */
		$widget_options = array(
			'classname'   => 'ps_box_sm_widget',
			'description' => esc_html__( 'Widget to add small boxes', 'printing-shop' )
		);

		/* Create the widget. */
		$this->WP_Widget(
			'ps-box-sm',               // $this->id_base
			__( 'Printing Shop Small Box', 'printing-shop' ), // $this->name
			$widget_options                   // $this->control_options
		);
	}

	/**
	 * Outputs the widget based on the arguments input through the widget controls.
	 *
	 * @since 1.0.0
	 */
	function widget( $sidebar, $instance ) {
		extract( $sidebar );/* Set up the default form values. */

		$defaults = array(
			'line1' => '',
			'line2' => '',
			'link' => '',
			'color'=> 'box-color-1'
		);

		/* Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $defaults );

		/* Output the theme's widget wrapper. */
		echo $before_widget; ?>

			<div class="sm-box-content <?php echo esc_attr( $instance['color'] ); ?>">
				<p>
					<a href="<?php echo esc_url( $instance['link']  ); ?>" class="ps-box-line1"><?php echo esc_html( $instance['line1']  ); ?></a>
					<a href="<?php echo esc_url( $instance['link']  ); ?>" class="ps-box-line2"><?php echo esc_html( $instance['line2'] ); ?></a>
				</p>
			</div>

		<?php 

		/* Close the theme's widget wrapper. */
		echo $after_widget;
	}

	/**
	 * Updates the widget control options for the particular instance of the widget.
	 *
	 * @since 1.0.0
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Set the instance to the new instance. */

		$instance['line1']    = strip_tags( $new_instance['line1'] );
		$instance['line2']    = strip_tags( $new_instance['line2'] );
		$instance['link']     = strip_tags( $new_instance['link'] );
		$instance['color']    = strip_tags( $new_instance['color'] );

		return $instance;
	}

	/**
	 * Displays the widget control options in the Widgets admin screen.
	 *
	 * @since 1.0.0
	 */
	function form( $instance ) {

		/* Set up the default form values. */
		$defaults = array(
			'line1' => '',
			'line2' => '',
			'link' => '',
			'color'=> 'box-color-1'
		);

		$colors = array( 
			'box-color-1',
			'box-color-2',
			'box-color-3',
			'box-color-4'
		);

		$color_skin = get_theme_mod( 'printing_shop_color_scheme', 'bluematte' );

		/* Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>

		<div class="hybrid-widget-controls <?php echo esc_attr( $color_skin ); ?>">
		<p class="widget-boxes-color-field">
			<label for="<?php echo $this->get_field_id( 'color' ); ?>"><?php _e( 'color:', 'printing-shop' ); ?></label><br />
			<?php foreach ( $colors as $option_value ) { ?>
				<input name="<?php echo $this->get_field_name( 'color' ); ?>" id="<?php echo $this->get_field_id( 'color' ); ?>-<?php echo esc_attr( $option_value ); ?>" type="radio" value="<?php echo esc_attr( $option_value ); ?>" <?php checked( $instance['color'], $option_value ); ?>> <label class="<?php echo esc_attr( $option_value ); ?>" for="<?php echo $this->get_field_id( 'color' ); ?>-<?php echo esc_attr( $option_value ); ?>"><?php echo esc_html( $option_value ); ?></label>
			<?php } ?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'line1' ); ?>"><?php _e( 'Line 1:', 'printing-shop' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'line1' ); ?>" name="<?php echo $this->get_field_name( 'line1' ); ?>" value="<?php echo esc_attr( $instance['line1'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'line2' ); ?>"><?php _e( 'Line 2:', 'printing-shop' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'line2' ); ?>" name="<?php echo $this->get_field_name( 'line2' ); ?>" value="<?php echo esc_attr( $instance['line2'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'link:', 'printing-shop' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo esc_attr( $instance['link'] ); ?>" />
		</p>
		</div>
		<div style="clear:both;">&nbsp;</div>
	<?php
	}
}