<?php
/**
 * Hooks and filters run in after_theme_setup
 *
 * @package PrintingShop
 * @subpackage Includes
 * @since 1.0.0
 */

/* Register menus. */
add_theme_support( 
	'hybrid-core-menus', 
	array( 'primary' ) 
);

/* Register sidebars. */
add_theme_support( 
	'hybrid-core-sidebars', 
	array( 'primary', 'subsidiary' ) 
);

/* Load styles. */
add_theme_support( 
	'hybrid-core-styles', 
	array( 'parent', 'style', 'printing-shop-fontawesome' ) 
);

/* Load widgets. */
add_theme_support( 'hybrid-core-widgets' );

/* Load shortcodes. */
add_theme_support( 'hybrid-core-shortcodes' );

/* Enable custom template hierarchy. */
add_theme_support( 'hybrid-core-template-hierarchy' );

/* Enable theme layouts (need to add stylesheet support). */
add_theme_support( 
	'theme-layouts', 
	array( '1c', '2c-l', '2c-r' ), 
	array( 'default' => '2c-r', 'customizer' => true ) 
);

/* Allow per-post stylesheets. */
add_theme_support( 'post-stylesheets' );

/* Support pagination instead of prev/next links. */
add_theme_support( 'loop-pagination' );

/* The best thumbnail/image script ever. */
add_theme_support( 'get-the-image' );

/* Use breadcrumbs. */
add_theme_support( 'breadcrumb-trail' );

/* Nicer [gallery] shortcode implementation. */
if( !class_exists( 'Jetpack' ) || !Jetpack::is_module_active( 'tiled-gallery' ) ) {
	add_theme_support( 'cleaner-gallery' );	
	add_filter( 'post_gallery', 'printing_shop_gallery', 99, 2 ); // magnific popup (lightbox)
}

/* Better captions for themes to style. */
add_theme_support( 'cleaner-caption' );

/* Automatically add feed links to <head>. */
add_theme_support( 'automatic-feed-links' );

/* Post formats. */
add_theme_support( 
	'post-formats', 
	array( 'aside', 'audio', 'chat', 'image', 'gallery', 'link', 'quote', 'status', 'video' ) 
);

/* Custom background. */
add_theme_support( 
	'custom-background'
);

/* Handle content width for embeds and images. */
hybrid_set_content_width( 780 );

/* footer. */
add_theme_support( 'hybrid-core-theme-settings', array( 'footer' ) );

/* media */
add_theme_support( 'hybrid-core-media-grabber' );

/*  theme fonts, custom version */
add_theme_support( 'djr-theme-fonts',   array( 'callback' => 'printing_shop_register_fonts', 'customizer' => true ) );

/* Register custom menus. */
add_action( 'init', 'printing_shop_register_menus', 11 );

/* Filter the sidebar widgets. */
add_filter( 'sidebars_widgets', 'printing_shop_disable_sidebars' );
add_action( 'template_redirect', 'printing_shop_theme_layout' );

/* Add custom image sizes. */
add_action( 'init', 'printing_shop_register_image_sizes' );

/* Register and load scripts. */
add_action( 'wp_enqueue_scripts', 'printing_shop_enqueue_scripts' );

/* Add respond.js and  html5shiv.js for unsupported browsers. */
add_action( 'wp_head', 'printing_shop_respond_html5shiv' );

/* Load custom styles. */
add_filter( "{$prefix}_styles", 'printing_shop_styles' );

/** extra sidebars/widget areas **/
add_action( 'widgets_init', 'printing_shop_register_sidebars', 11 );

/* Register additional widgets. */
add_action( 'widgets_init', 'printing_shop_register_widgets' );

/** remove breadcrumbs from home **/
add_filter( 'breadcrumb_trail', 'printing_shop_breadcrumb_trail' );
	
/* change excerpt length */
add_filter( 'excerpt_length', 'printing_shop_excerpt_length', 999 );

/* Modify excerpt more */
add_filter( 'excerpt_more', 'printing_shop_new_excerpt_more' );

/* add body_class filters */
add_filter( 'body_class', 'printing_shop_custom_body_classes' );

/* editor styles */
add_editor_style();

/* color support in editor */
add_filter( 'tiny_mce_before_init', 'printing_shop_tiny_mce_classes' );

/* add hybrid sidebar filters */
add_filter( "{$prefix}_sidebar_defaults", 'printing_shop_sidebar_defaults' );

/* filter front page */
add_filter( 'frontpage_template', 'printing_shop_front_page_template' );