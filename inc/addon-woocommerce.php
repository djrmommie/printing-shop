<?php
/**
 * Woocommerce Support
 *
 * @package PrintingShop
 * @subpackage Includes
 * @since 1.0.0
 */

/** theme support for woocommerce **/
add_theme_support( 'woocommerce' );

add_filter( 'woocommerce_enqueue_styles', '__return_false' );

/** breadcrumbs **/
add_filter( 'woocommerce_breadcrumb_defaults', 'printing_shop_woocommerce_breadcrumbs' );

/** move the product title to above image **/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5 );

/** filter pagination **/
add_filter( 'woocommerce_pagination_args', 'printing_shop_pagination_args' );

/** column filter for related products **/
add_filter('woocommerce_related_products_args', 'printing_shop_columns');

/** remove actions to simplify multi-product lists **/
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

add_filter( 'woocommerce_subcategory_count_html', '__return_empty_string' );

/** set number of products per page **/
add_filter( 'loop_shop_per_page', 'printing_shop_products_per_page', 20 );

/* custom image sizes. */
add_action( 'init', 'printing_shop_register_wc_image_sizes' );
add_filter( 'single_product_large_thumbnail_size', 'printing_shop_filter_single_image' );
add_filter( 'single_product_small_thumbnail_size', 'printing_shop_filter_category_image' );

/* Handle cart in header fragment for ajax add to cart */
add_filter('add_to_cart_fragments', 'printing_shop_header_add_to_cart_fragment');

/* Add the cart link to the header */
add_action( 'ps_header', 'printing_shop_header_cart_link', 10 );

/* Fix to bypass hybrid comments. */
add_action( 'woocommerce_before_main_content', 'printing_shop_modify_comments' );

/**
 * modify woocommerce breadcrumbs to match hybrid
 * @since  1.0.0
 * @param  array    $args default breadcrumb arguments
 * @return array
 */
if ( !function_exists('printing_shop_woocommerce_breadcrumbs') ) {
	function printing_shop_woocommerce_breadcrumbs( $args ) {

		$args['delimiter'] = ' > ';

		return $args;
	}
}

/**
 * filters woocommerce_pagination_args so that we can add next, previous to
 * match the pagination for standard posts, etc
 * @since  1.0.0		
 * @param  array    $args woocommerce default args for pagination
 * @return array
 */
if ( !function_exists('printing_shop_pagination_args') ) {
	function printing_shop_pagination_args( $args ) {

		$args['next_text'] = 'Next &rarr;';
		$args['prev_text'] = '&larr; Previous';

		return $args;
	}
}

/**
 * filters woocomerce columns to 4
 * @since  1.0.0	
 * @return integer
 */
if ( !function_exists('printing_shop_columns') ) {
	function printing_shop_columns( $args ) {
		$args['columns'] = 4; // 4 products per row
		return $args;
	}
}

if ( !function_exists( 'printing_shop_products_per_page' ) ) {
	/**
	 * Filters products per page.   Override pluggable function in your child theme to change.	
	 * @since  1.0.0
	 * @return int          new value for products per page
	 */
	function printing_shop_products_per_page() {
		return 16;
	}
}

/**
 * Add custom image size to use for single product page.  
 *
 * @since  1.0.0
 * @return void
 */
function printing_shop_register_wc_image_sizes() {

	/* image sizes */
	add_image_size( 'ps-shop-single', 415, 99999, false );
    add_image_size( 'ps-shop-single-full', 670, 99999, false );
	add_image_size( 'ps-shop-catalog', 150, 150, false );
    add_image_size( 'ps-shop-catalog-full', 300, 300, false );


}

/**
 * replace woocommerce 'shop_single' image size with custom
 * @since  1.0.0
 * @return string    registered image size
 */
function printing_shop_filter_single_image() {
    if ( 'layout-1c' == theme_layouts_get_layout() ) {
        $size = 'ps-shop-single-full';
    } else {
        $size = 'ps-shop-single';
    }
    return $size;
}

/**
 * replace woocommerce 'shop_catalog' image size with custom
 * @since  1.0.0
 * @return string    registered image size
 */
function printing_shop_filter_category_image() {
    if ( 'layout-1c' == theme_layouts_get_layout() ) {
        $size = 'ps-shop-catalog-full';
    } else {
        $size = 'ps-shop-catalog';
    }
    return $size;
}

/**
 * printing the cart header link
 * @since  1.0.0
 * @return void
 */
if ( ! function_exists( 'printing_shop_header_cart_link' ) ) {
	function printing_shop_header_cart_link() {
		echo printing_shop_woocommerce_cart_link();
	}
}

/**
 * adds cart link for ajax
 * @since  1.0.0
 * @param  string    $fragments
 * @return string
 */
function printing_shop_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	printing_shop_woocommerce_cart_link();

	$fragments['a.cart-button'] = ob_get_clean();

	return $fragments;

}

/**
 * prints the cart link for use in ajax fragments
 * @since  1.0.0
 * @return string
 */
function printing_shop_woocommerce_cart_link() {
	global $woocommerce;
	?>
	<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> <?php _e('in your shopping cart', 'printing-shop'); ?>" class="header-cart-button "><div class="cart">
			<span class="items"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'printing-shop'), $woocommerce->cart->cart_contents_count); ?></span>
			<span class="item-label"><?php echo printing_shop_item_label(); ?></span>
			<div class="total">
				<?php _e('Total: ', 'printing-shop'); ?>
				<?php echo $woocommerce->cart->get_cart_total();  ?>
			</div>	
	</div></a>
	<?php
}

/**
 * returns label for header cart icon according to user selection
 * @since  1.0.0
 * @return string    
 */
function printing_shop_item_label() {

	if ( 'cart' == get_theme_mod( 'printing_shop_woo_cart_icon', '' ) ) {
		$label =  _x('in your cart', 'cart widget', 'printing-shop');
	} else {
		$label =  _x('ready to print', 'cart widget', 'printing-shop');
	}
	return $label;

}

/**
 * echos style according to theme setting for cart icon
 * @since  1.0.0
 * @return string
 */
function pring_shop_cart_style() {

	if ( 'cart' == get_theme_mod( 'printing_shop_woo_cart_icon', '' ) ) {
		echo 'icon-cart';
	} else {
		echo 'icon-printer';
	}

}

/** woocomerce pluggable functions **/

if ( ! function_exists( 'woocommerce_output_related_products' ) ) {

	/**
	 * override woocomerce pluggable function to output 4 columns instead of 2
	 *
	 * @access public
	 * @subpackage	Product
	 * @return void
	 */
	function woocommerce_output_related_products() {
		woocommerce_related_products( array( 'posts_per_page' => 4, 'columns'  => 4 ) );
	}
}

if ( ! function_exists( 'woocommerce_upsell_display' ) ) {
	/**
	 * Output product up sells.  Override pluggable function to output 4 columns instead of 2
	 *
	 * @access public
	 * @param int $posts_per_page (default: -1)
	 * @param int $columns (default: 4)
	 * @param string $orderby (default: 'rand')
	 * @return void
	 */
	function woocommerce_upsell_display( $posts_per_page = '-1', $columns = 4, $orderby = 'rand' ) {
        wc_get_template( 'single-product/up-sells.php', array(
				'posts_per_page'  => $posts_per_page,
				'orderby'    => $orderby,
				'columns'    => $columns
			) );
	}
}

if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
	/**
	 * Get the product thumbnail, or the placeholder if not set.
	 *
	 * @access public
	 * @subpackage	Loop
	 * @param string $size (default: 'shop_catalog')
	 * @param int $placeholder_width (default: 0)
	 * @param int $placeholder_height (default: 0)
	 * @return string
	 */
	function woocommerce_get_product_thumbnail( $size = 'ps_shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
		global $post;

		if ( has_post_thumbnail() )
			return get_the_post_thumbnail( $post->ID, $size );
		elseif ( wc_placeholder_img_src() )
			return wc_placeholder_img( $size );
	}
}

/**
 * Removes hybrid_comments_template so that woocommerce will control comments_template
 * filter on products
 * @return null
 */
function printing_shop_modify_comments() {
	remove_filter( 'comments_template', 'hybrid_comments_template' );
}