<?php
/**
 * Posts widget
 *
 * @package PrintingShop
 * @subpackage Includes
 * @since 1.0.0
 */

/**
 * Post Widget Class
 *
 * @since 1.0.0
 */
class Printing_Shop_Post_Widget extends WP_Widget {

	/**
	 * Set up the widget's unique name, ID, class, description, and other options.
	 *
	 * @since 1.0.0
	 */
	function __construct() {

		/* Set up the widget options. */
		$widget_options = array(
			'classname'   => 'hot_off_press_widget',
			'description' => esc_html__( 'Widget to add post excepts', 'printing-shop' )
		);

		/* Set up the widget control options. */
		$control_options = array(
			'width'  => 525,
			'height' => 350
		);

		/* Create the widget. */
		$this->WP_Widget(
			'ps-posts',               // $this->id_base
			__( 'Printing Shop Hot Off the Press', 'printing-shop' ), // $this->name
			$widget_options,                   // $this->widget_options
			$control_options                   // $this->control_options
		);
	}

	/**
	 * Outputs the widget based on the arguments input through the widget controls.
	 *
	 * @since 1.0.0
	 */
	function widget( $sidebar, $instance ) {
		extract( $sidebar );

		/* Set up the default form values. */
		$defaults = array(
			'title'              => esc_attr__( 'Hot Off the Press', 'printing-shop' ),
			'taxonomy'           => 'category',
			'term'      	     => '',
			'include'            => '',
			'exclude'            => '',
			'order'              => 'DESC',
			'orderby'            => 'date',
			'limit'              => '3',
		);

		/* Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $defaults );

		/* Output the theme's widget wrapper. */
		echo $before_widget;

		/* If a title was input by the user, display it. */
		if ( !empty( $instance['title'] ) )
			echo $before_title . apply_filters( 'widget_title',  $instance['title'], $instance, $this->id_base ) . $after_title;

		$args = array(
				'posts_per_page' => $instance['limit'],
				'ignore_sticky_posts' => 1

			);

		if ( $instance['term'] ) {
			$args['tax_query'] = array(
				array(
						'taxonomy' => $instance['taxonomy'],
						'field' => 'id',
						'terms' => $instance['term']
				)
			);
		}

		if ( $instance['include'] ) {
			$args['post__in'] = explode( ',' , $instance['include'] ); 
		}

		if ( $instance['exclude'] ) {
			$args['post__not_in'] = explode( ',' , $instance['exclude'] ); 
		}

		if ( 'ASC' == $instance['order'] ) {
			$args['order'] = 'ASC'; 
		}

		if ( 'name' == $instance['orderby'] ) {
			$args['orderby'] = 'title'; 
		} elseif ( 'list' == $instance['orderby'] ) {
			$args['orderby'] = 'post__in'; 
		}

		$loop = new WP_Query( $args );

		if ( $loop->have_posts() ) {

			echo '<div class="hot-off-press-features">';

			while ( $loop->have_posts() ) {
				$loop->the_post();

					echo '<div><div>';

						$color_skin = get_theme_mod( 'printing_shop_color_scheme', 'bluematte' );
						$default_thumbnail = hybrid_locate_theme_file( 'images/featured-image-' . $color_skin . '.png' ); ?>

						<header class="entry-header">
							<?php if ( current_theme_supports( 'get-the-image' ) ) get_the_image( array( 'size'=>'post-thumbnail', 'meta_key_save'=>true, 'default_image'=>$default_thumbnail ) ); ?>
							<?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
						</header><!-- .entry-header -->

						<div class="entry-summary">
							<?php the_excerpt(); ?>
							<span class="ps-read-more"><a class="more-link" href="<?php echo get_permalink(); ?>" title="<?php the_title_attribute('echo=0'); ?>">  <?php _e( 'Read More', 'printing-shop' ); ?></a></span>
						</div><!-- .entry-summary -->

					</div></div>

				<?php } // end while

			echo '</div>';
		}

		wp_reset_query();

		/* Close the theme's widget wrapper. */
		echo $after_widget;
	}

	/**
	 * Updates the widget control options for the particular instance of the widget.
	 *
	 * @since 1.0.0
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Set the instance to the new instance. */
		$instance = $new_instance;

		/* If new taxonomy is chosen, reset includes and excludes. */
		if ( $instance['taxonomy'] !== $old_instance['taxonomy'] && '' !== $old_instance['taxonomy'] ) {
			$instance['include'] = array();
			$instance['exclude'] = array();
		}

		$instance['taxonomy'] = $new_instance['taxonomy'];
		$instance['term']     = intval($new_instance['term']);
		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['include']  = preg_replace( '/[^0-9,]/', '', $new_instance['include'] );
		$instance['exclude']  = preg_replace( '/[^0-9,]/', '', $new_instance['exclude'] );
		$instance['limit']    = intval($new_instance['limit'] );

		return $instance;
	}

	/**
	 * Displays the widget control options in the Widgets admin screen.
	 *
	 * @since 0.6.0
	 */
	function form( $instance ) {

		/* Set up the default form values. */
		$defaults = array(
			'title'              => esc_attr__( 'Hot Off the Press', 'printing-shop' ),
			'taxonomy'           => 'category',
			'term'           => '',
			'include'            => '',
			'exclude'            => '',
			'order'              => 'DESC',
			'orderby'            => 'date',
			'limit'             => '3',
		);

		/* Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $defaults );

		/* get available taxonomies */
		//$taxonomies =  get_object_taxonomies( 'post' , 'objects') ;
		
		/* get available terms */
		$terms = get_terms( $instance['taxonomy'] );

		$order = array( 
			'ASC'  => esc_attr__( 'Ascending', 'printing-shop' ), 
			'DESC' => esc_attr__( 'Descending', 'printing-shop' ) 
		);

		$orderby = array( 
			'date'         => esc_attr__( 'Date', 'printing-shop' ), 
			'name'       => esc_attr__( 'Name', 'printing-shop' ), 
			'list'       => esc_attr__( 'Include List Order', 'printing-shop' )
		);

		?>

		<div class="hybrid-widget-controls columns-2">
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'printing-shop' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><code>order</code></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>">
				<?php foreach ( $order as $option_value => $option_label ) { ?>
					<option value="<?php echo esc_attr( $option_value ); ?>" <?php selected( $instance['order'], $option_value ); ?>><?php echo esc_html( $option_label ); ?></option>
				<?php } ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'orderby' ); ?>"><code>orderby</code></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'orderby' ); ?>" name="<?php echo $this->get_field_name( 'orderby' ); ?>">
				<?php foreach ( $orderby as $option_value => $option_label ) { ?>
					<option value="<?php echo esc_attr( $option_value ); ?>" <?php selected( $instance['orderby'], $option_value ); ?>><?php echo esc_html( $option_label ); ?></option>
				<?php } ?>
			</select>
		</p>
		</div>

		<div class="hybrid-widget-controls columns-2 column-last">
		<input type="hidden" id="<?php echo $this->get_field_id( 'taxonomy' ); ?>" name="<?php echo $this->get_field_name( 'taxonomy' ); ?>" value="category" />
		<?php /*
		<p>
			<label for="<?php echo $this->get_field_id( 'taxonomy' ); ?>"><code>taxonomy</code></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'taxonomy' ); ?>" name="<?php echo $this->get_field_name( 'taxonomy' ); ?>">
				<?php foreach ( $taxonomies as $taxonomy ) { ?>
					<option value="<?php echo esc_attr( $taxonomy->name ); ?>" <?php selected( $instance['taxonomy'], $taxonomy->name ); ?>><?php echo esc_html( $taxonomy->labels->singular_name ); ?></option>
				<?php } ?>
			</select>
		</p>
		*/ ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'term' ); ?>"><code>term</code></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'term' ); ?>" name="<?php echo $this->get_field_name( 'term' ); ?>">
				<option value="" <?php selected( $instance['term'], '' ); ?>>-</option>
				<?php foreach ( $terms as $term ) { ?>
					<option value="<?php echo esc_attr( $term->term_id ); ?>" <?php selected( $instance['term'], $term->term_id ); ?>><?php echo esc_html( $term->name ); ?></option>
				<?php } ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'limit' ); ?>"><code>limit</code></label>
			<input type="text" class="smallfat code" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" value="<?php echo esc_attr( $instance['limit'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'include' ); ?>"><code>include</code></label>
			<input type="text" class="smallfat code" id="<?php echo $this->get_field_id( 'include' ); ?>" name="<?php echo $this->get_field_name( 'include' ); ?>" value="<?php echo esc_attr( $instance['include'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'exclude' ); ?>"><code>exclude</code></label>
			<input type="text" class="smallfat code" id="<?php echo $this->get_field_id( 'exclude' ); ?>" name="<?php echo $this->get_field_name( 'exclude' ); ?>" value="<?php echo esc_attr( $instance['exclude'] ); ?>" />
		</p>
		</div>
		<div style="clear:both;">&nbsp;</div>
	<?php
	}
}

?>