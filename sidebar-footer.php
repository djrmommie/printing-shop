<?php if ( is_active_sidebar( 'footer-widgets' ) ) { ?>

	<aside id="sidebar-footer" class="sidebar">

		<?php dynamic_sidebar( 'footer-widgets' ); ?>

	</aside><!-- #sidebar-footer .aside -->

<?php } ?>