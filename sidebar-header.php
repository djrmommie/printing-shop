<?php 

if ( is_active_sidebar( 'header-widgets' ) || class_exists( 'woocommerce' ) ) { ?>

	<aside id="sidebar-header" class="sidebar header-sidebar">

		<?php dynamic_sidebar( 'header-widgets' ); ?>
		<?php if ( class_exists( 'woocommerce' ) ) { ?>
		<aside id="header-shopping-cart" class="widget <?php pring_shop_cart_style(); ?>">
			<div class="widget-wrap widget-inside">
				<?php do_action( 'ps_header' ); ?>
			</div>
		</aside>
		<?php } ?>

	</aside><!-- #sidebar-header .aside -->

<?php }